#include<stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include<unistd.h>
#include<stdlib.h>

int mysystem(char *cmd)
{
    int status,pid;
    pid = fork();
    if(pid==-1)
        return -1;

    if(pid == 0)
    {
        execl("/bin/sh","sh","-c",cmd,0);
        exit(111);
    }
    if(pid > 0)
    {
        waitpid(pid,&status,0);
        printf("Completed\n");
    }
    return status;
}

int main(int argc, char** argv)
{
    int pid,x;
    x = mysystem(argv[1]);
    switch(x)
    {
        case -1 : printf("error\n");break;
        case 111: printf("exec failure\n");
    }
    return 0;
}
