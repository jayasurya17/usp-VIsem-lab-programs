#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
int main()
{
    int pid;
    printf("This is parent\n");
    pid = fork();
    if(pid == 0)
    {
        printf("This is child\nMy pid is %d\n",getpid());
        pid = fork();
        if(pid == 0)
            exit(0);
        printf("I am second child, My id is %d\n",getpid());
        printf("I am second child, My parent id is %d\n",getppid());
    }
    return 0;
}
