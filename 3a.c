#include<stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include<unistd.h>
#include <sys/stat.h>
#include<stdlib.h>
#include<fcntl.h>
int main(int argc,char **argv)
{
    int fd;
    struct flock p;
    fd = open("file1",O_RDWR);
    p.l_type = F_WRLCK;
    p.l_whence = SEEK_CUR;
    p.l_start = 0;
    p.l_len=100;
    fcntl(fd,F_SETLK,&p);
    printf("%d has acquired lock\n",getpid());
    sleep(10);
    p.l_type = F_UNLCK;
    fcntl(fd,F_SETLK,&p);
    printf("P1 has released lock\n");
    return 0;
}
