#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int pid;
    pid = fork();
    if(pid > 0)
    {
        sleep(10);
        system("ps -l");
    }
    if(pid == 0)
    {
        printf("Child process\n");
        printf("My PID id %d\n",getpid());
        exit(1);
    }
    return 0;
}
