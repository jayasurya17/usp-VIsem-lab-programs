#define _POSIX_SOURCE
#include <stdio.h>
#include <unistd.h>
int main()
{
    #ifdef _POSIX_JOB_CONTROL
        printf("The system supports BSD type job control\n");
    #else
        printf("The system doesn't support BSD type job control\n");
    #endif

    #ifdef _POSIX_SAVED_IDS
        printf("The system supports saved set_uid and set_gid\n");
    #else
        printf("The system doesn't support saved set_uid and set_gid\n");
    #endif
    
    #ifdef _POSIX_VDISABLE
    printf("Disable char for terminal file is %d\n",_POSIX_VDISABLE);
    #else
    printf("The system doesn't support disable char\n");
    #endif

    #ifdef _POSIX_CHOWN_RESTRICTED
        printf("Chown restricted option is %d\n",_POSIX_CHOWN_RESTRICTED);
    #else
        printf("The system doesn't support chown restricted\n");
    #endif

    #ifdef _POSIX_NO_TRUNC
        printf("Pathname trunc option is %d\n",_POSIX_NO_TRUNC);
    #else
        printf("The system doesn't support pathname trunc\n");
    #endif

}
