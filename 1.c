#define _POSIX_SOURCE
#include <stdio.h>
#include <unistd.h>
int main()
{
    int res;

    if((res == sysconf(_SC_CLK_TCK)) == -1)
        perror("sysconf");
    else
        printf("No of clock ticks = %d\n",res);

    if((res == sysconf(_SC_CHILD_MAX)) == -1)
        perror("sysconf");
    else
        printf("No of child process = %d\n",res);

    if((res == sysconf(_SC_OPEN_MAX)) == -1)
        perror("sysconf");
    else
        printf("No of open files = %d\n",res);

    if((res == pathconf("/",_PC_PATH_MAX)) == -1)
        perror("sysconf");
    else
        printf("Max path length = %d\n",res+1);

    if((res == pathconf("/",_PC_NAME_MAX)) == -1)
        perror("pathconf");
    else
        printf("Max filename = %d\n",res+1);
}
