#include <stdio.h>
#include <unistd.h>
void display(char *s)
{
    char *p,c;
    p = s;
    while(c = *p++)
    {
        putc(c,stdout);
        fflush(stdout);
        sleep(1);
    }
}
int main()
{
    int pid;
    if((pid == fork()) == 0)
        display("I am child\n");
    else
        display("I am parent\n");
    return 0;
}
