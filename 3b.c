#include<stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include<unistd.h>
#include <sys/stat.h>
#include<stdlib.h>
#include<fcntl.h>
int main(int argc,char **argv)
{
    int fd; char buf[50];
    struct flock s;
    fd = open("file1",O_RDWR);
    s.l_type = F_WRLCK;
    s.l_whence = SEEK_CUR;
    s.l_start=0;
    s.l_len = 100;
    fcntl(fd,F_SETLK,&s);
    while(fcntl(fd,F_SETLK,&s)==-1)
    {
        fcntl(fd,F_GETLK,&s);
        printf("File is locked by %d\n",s.l_pid);
        sleep(1);
    }
    printf("I got lock\n");
    lseek(fd,-50,SEEK_END);
    read(fd,buf,50);
    printf("%s\n",buf);
    s.l_type = F_UNLCK;
    fcntl(fd,F_SETLK,&s);
    printf("Released lock\n");
    return 0;
}
